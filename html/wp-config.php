<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'techlab' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'T4x ~UUh<FB&g6Logy|>c?2/e{8IF__3R)q2da0B &_WT}QBL2C/<#fIne]YSGDk' );
define( 'SECURE_AUTH_KEY',  'S^Uy7M6KHf%1}(}=L:Gm#^KA%@0$@OL4 c+(>*rh<G#mb%x{7dJa9z!wT0BIc<Gw' );
define( 'LOGGED_IN_KEY',    '>oh#9OG%3O4iz&l}rvu9%rPaN>_N/oK5nUjY|+8g340z,8e@# ~dZcy79x84tk+>' );
define( 'NONCE_KEY',        'W|riD;+n% BU57zn L^5[)a]y7FWC-$dix!fVClWRbr;}(/{cf:;j%Ks:tkCbl{9' );
define( 'AUTH_SALT',        '9Sjv&KrK4K=hZc,Kk[Sj.vv-.{[=AqdrSzPHXUg?)/!$%:#yHGk|-h;(f6pAv~gg' );
define( 'SECURE_AUTH_SALT', '7s%Rf226#)Wed/N(H1j[OD`N1TdN3N&vD&tK#,{fO}>_nSGE{CxwbR~T$>{#mc@M' );
define( 'LOGGED_IN_SALT',   'vSgWOuO)?Y3)JNr<*gSN8EEIjBmN=KdWW|fH4tuMk_KqWr8,;)w`i(2AWI7QEprW' );
define( 'NONCE_SALT',       'mI>A]K3o.=m`!lBJ!0:~>T)WMdamiJ#~H[T+.B7,=XJ:~~89`[EBsRWa%4Y2u}bp' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
